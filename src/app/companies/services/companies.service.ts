import { Company } from './../model/company';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay, first, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  private readonly API = 'http://localhost:8080/api'

  constructor( private httpClient: HttpClient) { }

  listCompanies(){
    return this.httpClient.get<Company[]>(`${this.API}/company`)
    .pipe(
      first()
    );
  }

  createCompany(company: any){
    return this.httpClient.post<Company[]>(`${this.API}/company`, company)
  }

  updateCompany(idCompany: any, company: any){

    console.log('aaa', idCompany, company);
    return this.httpClient.put<Company[]>(`${this.API}/company/${idCompany}`, company)
    .pipe(
      first(),
      tap(companies => console.log(companies)
      )
    );
  }

  deleteCompany(idCompany: any){
    return this.httpClient.delete<Company>(`${this.API}/company/${idCompany}`)
  }
}
