import { CompaniesService } from '../services/companies.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Accountable } from 'src/app/accountable/model/accountable';
import { AccountableService } from 'src/app/accountable/services/accountable.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dialog-create',
  templateUrl: './dialog-create.component.html',
  styleUrls: ['./dialog-create.component.scss']
})
export class DialogCreateComponent implements OnInit {
  liveForm: FormGroup;
  accountable: Observable<Accountable[]>;
  selectedValue: string = '';

  constructor(
    private companiesService: CompaniesService,
    private accountableService: AccountableService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogCreateComponent>
  ) {

    this.liveForm = this.fb.group(
      {
        trading_name: ['', [Validators.required]],
        fantasy_name: ['', [Validators.required]],
        cnpj: ['', [Validators.required]],
        id_accountable: ['', [Validators.required]],
      }
    )

    this.accountable = this.accountableService.listAccountable()
    .pipe();
   }

  ngOnInit(): void {
  }

  cancel(): void {
    this.dialogRef.close();
    this.liveForm.reset();
  }

  addNewCompany(){
    this.companiesService.createCompany(this.liveForm.value).subscribe(result => {
      window.location.reload();
    });
    this.dialogRef.close();
    this.liveForm.reset();
  }

}
