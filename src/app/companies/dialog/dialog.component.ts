//dialog-box.component.ts
import { Component, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CompaniesService } from '../services/companies.service';

export interface CompanyData {
  name: string;
  id: number;
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {

  action:string;
  local_data:any;

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    private companiesService: CompaniesService,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: CompanyData)
  {
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  doAction(){
    if(this.action == 'Editar'){
      this.companiesService.updateCompany(this.local_data.id, this.local_data).subscribe(result => {
        window.location.reload();
      });
    }

    if(this.action == 'Deletar'){
      this.companiesService.deleteCompany(this.local_data.id).subscribe(result => {
        window.location.reload();
      });
    }

    this.dialogRef.close();
  }

  closeDialog(){
    this.dialogRef.close();
  }

}
