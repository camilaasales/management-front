import { DialogCreateComponent } from './../dialog-create/dialog-create.component';
import { ErrorDialogComponent } from './../../shared/components/error-dialog/error-dialog.component';
import { Company } from './../model/company';
import { Component, OnInit } from '@angular/core';
import { CompaniesService } from '../services/companies.service';
import { catchError, Observable, of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})

export class CompaniesComponent implements OnInit {

  companies: Observable<Company[]>;
  displayedColumns = ['trading_name', 'fantasy_name', 'cnpj', 'accountable_name', 'actions'];


  constructor(
    private companiesService: CompaniesService,
    public dialog: MatDialog
  ) {
    this.companies = this.companiesService.listCompanies()
    .pipe(
      catchError(error => {
        this.onError('erro ao carregar empresas')
        return of([]);
      })
    );
  }

  onError(erroMsg: string) {
    this.dialog.open(ErrorDialogComponent, {
      data: erroMsg,
    });
  }

  ngOnInit(): void {
  }

  addNewCompany(): void {
    const dialogRef = this.dialog.open(DialogCreateComponent, {
      minWidth: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialog(action: any, obj: any) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogComponent, {
      minWidth: '400px',
      data:obj
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   window.location.reload();
    //   console.log(result, 'aa');

      // if(result.event == 'Editar'){
      //   this.updateRowData(result.data);
      // }else if(result.event == 'Deletar'){
      //   this.deleteRowData(result.data);
      // }
    // });
  }

  // updateRowData(row_obj){
  //   this.companies = this.companies.filter((value,key)=>{
  //     if(value.id == row_obj.id){
  //       value.name = row_obj.name;
  //     }
  //     return true;
  //   });
  // }
  // deleteRowData(row_obj){
  //   this.companies = this.companies.filter((value,key)=>{
  //     return value.id != row_obj.id;
  //   });
  // }

}
