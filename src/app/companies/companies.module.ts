import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AppMaterialModule } from '../shared/app-material/app-material.module';
import { SharedModule } from './../shared/shared.module';
import { CompaniesRoutingModule } from './companies-routing.module';
import { CompaniesComponent } from './companies/companies.component';
import { DialogCreateComponent } from './dialog-create/dialog-create.component';
import { DialogComponent } from './dialog/dialog.component';



@NgModule({
  declarations: [
    CompaniesComponent,
    DialogCreateComponent,
    DialogComponent
  ],
  imports: [
    CommonModule,
    CompaniesRoutingModule,
    AppMaterialModule,
    SharedModule
  ]
})
export class CompaniesModule { }
