export interface Company {
  id: string;
  trading_name: string;
  fantasy_name: string;
  cnpj: string;
}

