import { AccountableService } from './../services/accountable.service';
import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface AccountableData {
  name: string;
  id: number;
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent{

  action:string;
  local_data:any;

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    private accountableService: AccountableService,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: AccountableData) {
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  doAction(){
    if(this.action == 'Editar'){
      this.accountableService.updateAccountable(this.local_data.id, this.local_data).subscribe(result => {
        window.location.reload();
      });
    }

    if(this.action == 'Deletar'){
      this.accountableService.deleteAccountable(this.local_data.id).subscribe(result => {
        window.location.reload();
      });
    }

    this.dialogRef.close();
  }

  closeDialog(){
    this.dialogRef.close();
  }

}
