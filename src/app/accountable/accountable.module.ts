import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AppMaterialModule } from '../shared/app-material/app-material.module';
import { SharedModule } from './../shared/shared.module';
import { AccountableRoutingModule } from './accountable-routing.module';
import { AccountableComponent } from './accountable/accountable.component';
import { DialogCreateComponent } from './dialog-create/dialog-create.component';
import { DialogComponent } from './dialog/dialog.component';

@NgModule({
  declarations: [
    AccountableComponent,
    DialogComponent,
    DialogCreateComponent
  ],
  imports: [
    CommonModule,
    AccountableRoutingModule,
    SharedModule,
    AppMaterialModule
  ]
})
export class AccountableModule { }
