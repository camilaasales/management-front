import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay, first, tap } from 'rxjs';
import { Accountable } from '../model/accountable';

@Injectable({
  providedIn: 'root'
})
export class AccountableService {

  private readonly API = 'http://localhost:8080/api'

  constructor( private httpClient: HttpClient) { }

  listAccountable(){
    return this.httpClient.get<Accountable[]>(`${this.API}/accountable`)
    .pipe(
      first()
    );
  }

  createAccountable(accountable: any){
    return this.httpClient.post<Accountable[]>(`${this.API}/accountable`, accountable)
  }

  updateAccountable(idAccountable: any, accountable: any){

    console.log('aaa', idAccountable, accountable);
    return this.httpClient.put<Accountable[]>(`${this.API}/accountable/${idAccountable}`, accountable)
    .pipe(
      first(),
      tap(companies => console.log(companies)
      )
    );
  }

  deleteAccountable(idAccountable: any){
    return this.httpClient.delete<Accountable>(`${this.API}/accountable/${idAccountable}`)
  }
}
