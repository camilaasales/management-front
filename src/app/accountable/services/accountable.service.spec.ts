import { TestBed } from '@angular/core/testing';

import { AccountableService } from './accountable.service';

describe('AccountableService', () => {
  let service: AccountableService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
