export interface Accountable {
  id: string;
  name: string;
  email: string;
  cpf: string;
}
