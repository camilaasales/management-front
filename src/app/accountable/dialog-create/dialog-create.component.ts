import { AccountableService } from './../services/accountable.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-create',
  templateUrl: './dialog-create.component.html',
  styleUrls: ['./dialog-create.component.scss']
})
export class DialogCreateComponent implements OnInit {
  liveForm: FormGroup;
  constructor(
    private accountableService: AccountableService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogCreateComponent>
  ) {

    this.liveForm = this.fb.group(
      {
        name: ['', [Validators.required]],
        email: ['', [Validators.required]],
        cpf: ['', [Validators.required]],
      }
    )
  }

  ngOnInit(): void {
  }

  cancel(): void {
    this.dialogRef.close();
    this.liveForm.reset();
  }

  addNewAccountable(){
    this.accountableService.createAccountable(this.liveForm.value).subscribe(result => {
      window.location.reload();
    });
    this.dialogRef.close();
    this.liveForm.reset();
  }

}
