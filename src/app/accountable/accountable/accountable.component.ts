import { ErrorDialogComponent } from './../../shared/components/error-dialog/error-dialog.component';
import { AccountableService } from './../services/accountable.service';
import { Accountable } from './../model/accountable';
import { Component, OnInit } from '@angular/core';
import { catchError, Observable, of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { DialogCreateComponent } from '../dialog-create/dialog-create.component';

@Component({
  selector: 'app-accountable',
  templateUrl: './accountable.component.html',
  styleUrls: ['./accountable.component.scss']
})
export class AccountableComponent implements OnInit {

  accountable: Observable<Accountable[]>;
  displayedColumns = ['name', 'email', 'cpf', 'actions'];


  constructor(
    private accountableService: AccountableService,
    public dialog: MatDialog
  ) {
    this.accountable = this.accountableService.listAccountable()
    .pipe(
      catchError(error => {
        this.onError('erro ao carregar responsáveis')
        return of([]);
      })
    );
  }

  onError(erroMsg: string) {
    this.dialog.open(ErrorDialogComponent, {
      data: erroMsg,
    });
  }


  ngOnInit(): void {
  }

  addNewAccountable(): void {
    const dialogRef = this.dialog.open(DialogCreateComponent, {
      minWidth: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialog(action: any, obj: any) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogComponent, {
      minWidth: '400px',
      data:obj
    });

  }

}
