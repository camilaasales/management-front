# Angular CLI - CRUD


## O que é este projeto?
O projeto tem como objetivo utilizar a API em Laravel para realizar o CRUD para a empresa e seu resposável.


## Pré-requisitos
Para começar a utilizar o **PO UI** é pré-requisito ter o `Node.js` instalado (versão 12.x ou acima) e o seu gerenciador de pacote favorito na versão mais atual. Caso você ainda não tenha instalado o pacote `@angular/cli`, instale-o via `npm` ou `yarn`.

Instalando com npm:
```
npm i -g @angular/cli@^13
```

Caso prefira instalar com o yarn:
```
yarn global add @angular/cli@^13
```

## Para rodar este projeto
```bash
$ git clone https://CamilaASales@bitbucket.org/camilaasales/management-front.git
$ cd management-front
$ npm install
$ ng serve
```
Acesssar pela url: http://localhost:4200/


## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
